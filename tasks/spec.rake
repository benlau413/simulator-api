namespace :spec do
  require 'rspec/core/rake_task'
  desc "Run all specs for CI"
  RSpec::Core::RakeTask.new(:ci) do |spec|
    spec.rspec_opts = '-c --format documentation --format html --out reports/spec.html'
  end

  desc "Run unit test specs"
  RSpec::Core::RakeTask.new(:unit) do |t|
    # ENV['DATABASE_URL'] ||= 'sqlite://migrations/test.db'
    t.pattern = Dir['spec/unit/**/*_spec.rb'].reject{ |f| f['/functional'] }
  end

  desc "Run functional test specs"
  RSpec::Core::RakeTask.new(:functional) do |t|
    t.pattern = "spec/functional/**/*_spec.rb"
  end
end
