require_relative '../lib/simulator-api.rb'

action_group = ["shuffle_end", "shuffle_begin", "burn_card", "game_card", "cut_card"]
shoe_box_id = "abd321123"
card = "02d"

action_group.each do |action|
  if action == "burn_card" || action == "game_card"
    message = {
      action: action,
      shoe_box_id: shoe_box_id,
      card: card
    }
  else
    message = {
      action: action,
      shoe_box_id: shoe_box_id
    }
  end
  SimulatorApi.shoe_input_live_baccarat_kafka(message)
end
