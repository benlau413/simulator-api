require 'multi_json'
require 'ruby-kafka'

require_relative 'simulator-api/version'
require_relative 'simulator-api/error'
require_relative 'simulator-api/shoe_message'
require_relative 'simulator-api/shoe_action'
require_relative 'simulator-api/requesters'

module SimulatorApi
  module ClassMethod

    attr_writer :kafka_host
    attr_writer :idc_codename

    DEFAULT_KAFKA_HOST = 'http://0.0.0.0:9092'.freeze
    DEFAULT_IDC_CODEMANE = 'test'.freeze

    def kafka_host
      @kafka_host || DEFAULT_KAFKA_HOST
    end

    def idc_codename
      @idc_codename || DEFAULT_IDC_CODEMANE
    end

    def logger
      @logger
    end

    NECESSARY_INPUT_KEYS = %w[action shoe_box_id]

    def shoe_input_live_baccarat_kafka(message)
      NECESSARY_INPUT_KEYS.each do |key|
        raise SimulatorApi::ParameterMismatchError.new("Simulator-Api input: missing key (#{key.to_s})") if message[key].nil?
      end
      SimulatorApi::ShoeMessage.send_kafka(
        action: message["action"],
        shoe_box_id: message["shoe_box_id"],
        card: message["card"]
      )
    end
  end

  extend ClassMethod
end
