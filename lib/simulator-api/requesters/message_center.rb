# frozen_string_literal: true

module SimulatorApi
  module Requesters
    module MessageCenter
      extend self

      def bootup(shoe_box_id)
        brokers = SimulatorApi.kafka_host
        client_id = "simulator_" + shoe_box_id
        @kafka = Kafka.new(brokers, client_id: client_id)
      end

      def kafka_produce_message(kafka_message)
        bootup(kafka_message[:shoe_box_id]) if @kafka.nil?
        @producer = @kafka.async_producer
        topic = construct_topic(kafka_message[:shoe_box_id])
        @producer.produce(
          MultiJson.dump(kafka_message),
          topic: topic
        )
        @producer.deliver_messages
        @producer.shutdown
        kafka_info = {
          kafka_host: SimulatorApi.kafka_host,
          topic: topic
        }
        SimulatorApi.logger.info("kafka_info = #{kafka_info}")
        kafka_info
      end

      def construct_topic(topic)
        "#{SimulatorApi.idc_codename}_#{topic}"
      end
    end
  end
end
