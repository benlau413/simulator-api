# frozen_string_literal: true

module SimulatorApi
  class ShoeMessage
    FACE_VALUE = ['A', '2', '3', '4', '5', '6', '7', '8', '9', 'T', 'J', 'Q', 'K']
    UNLOCK_SHOE = "unlock_shoe"
    LOCK_SHOE = "lock_shoe"
    DRAW_CARD = "draw_card"
    BLACK_CARD = "black_card"

    class << self
      def send_kafka(action:, shoe_box_id:, card: nil)
        case action
        when "shuffle_end"
          unlock_shoe_action(shoe_box_id)
        when "shuffle_begin"
          lock_shoe_action(shoe_box_id)
        when "burn_card", "game_card"
          raise SimulatorApi::ParameterMismatchError.new("Simulator-Api input: draw card action mssing key (card)") if card.nil?
          card_number = card[0].to_i * 10 + card[1].to_i
          our_card = FACE_VALUE[card_number - 1] + card[2]
          draw_shoe_by_input_option(shoe_box_id, our_card)
        when "cut_card"
          our_card = BLACK_CARD
          draw_shoe_by_input_option(shoe_box_id, our_card)
        else
          raise SimulatorApi::ParameterMismatchError.new("Simulator-Api input: undefined action (#{action})")
        end
      end

      def unlock_shoe_action(shoe_box_id)
        SimulatorApi::ShoeAction.process(action: UNLOCK_SHOE, shoe_box_id: shoe_box_id)
      end

      def lock_shoe_action(shoe_box_id)
        SimulatorApi::ShoeAction.process(action: LOCK_SHOE, shoe_box_id: shoe_box_id)
      end

      def draw_shoe_by_input_option(shoe_box_id, card)
        SimulatorApi::ShoeAction.process(action: DRAW_CARD, shoe_box_id: shoe_box_id, card: card)
      end
    end
  end
end
