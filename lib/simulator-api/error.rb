module SimulatorApi
  class Error < StandardError
    class << self
      def from_response(body)
        err_hash = parse_error(body)
        new(err_hash[:description], err_hash[:error_code], err_hash[:code])
      end

      private

      def parse_error(body)
        MultiJson.load(body, symbolize_keys: true)
      end
    end

    # @return [String]
    attr_reader :code

    # @return [String]
    attr_reader :error_code

    # @return [String]
    attr_reader :description

    def initialize(description = '', error_code = nil, code = nil)
      super(description)
      @code = code
      @error_code = error_code
      @description = description
    end
  end

  class ParameterMismatchError < Error; end

end
