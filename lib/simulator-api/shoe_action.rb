# frozen_string_literal: true

module SimulatorApi
  class ShoeAction
    class << self
      def process(action:, shoe_box_id:, card: nil)
        kafka_message = build_kafka_message(action, shoe_box_id, card)
        kafka_info = kafka_produce_message(kafka_message)
        compose_shoe_action_result(kafka_message, kafka_info)
      end


      def build_kafka_message(action, shoe_box_id, card)
        kafka_message = {
          action: action,
          shoe_box_id: shoe_box_id
        }

        if kafka_message[:action] == SimulatorApi::ShoeMessage::DRAW_CARD
          kafka_message.merge!(
            'card': card
          )
        end
        SimulatorApi.logger.info("kafka_message = #{kafka_message}")
        kafka_message
      end

      def kafka_produce_message(kafka_message)
        kafka_info = SimulatorApi::Requesters::MessageCenter.kafka_produce_message(kafka_message)
      end


    ## for result
      def compose_shoe_action_result(kafka_message, kafka_info)
        result = {
          code: "OK"
        }
      end
    end
  end
end