# frozen_string_literal: true

require_relative 'requesters/message_center'

# load requesters code stuffs in /requesters folder
Dir["#{File.dirname(__FILE__)}/requesters/**.rb"]
  .each { |file| require_relative file }
