require File.expand_path('lib/simulator-api/version', __dir__)
# $LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)

Gem::Specification.new do |spec|
  spec.name          = 'simulator-api'
  spec.version       = SimulatorApi::VERSION
  spec.authors       = ['Ben Lao']
  spec.email         = ['ben.lao@gamesourcecloud.com']

  spec.summary       = 'API Client For Live Baccarat Simulator'
  spec.description   = 'A simple client for Simulator Remote API'
  spec.homepage      = 'http://home.rnd.laxino.com'

  spec.require_paths = ['lib']
  spec.files         = `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  spec.add_dependency  'bundler',      '>= 1.17'
  spec.add_dependency  'rake',         '>= 12.3.0'
  spec.add_dependency  'oj',           '>= 3.7.9'
  spec.add_dependency  'multi_json',   '>= 1.13.1'
  spec.add_dependency  'ruby-kafka',   '>= 1.3'
end
